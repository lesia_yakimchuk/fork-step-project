// dropdown navigation
$(document).ready(() => {
    let active = false;
    
    $('.burger').click(() => {
        active = !active;
        if (active) {
           $('.header__sandwich').css('display', 'none'); 
           $('.header__close').css('display', 'block');
           $('.header__nav').css('display', 'block');
        } else {
            $('.header__sandwich').css('display', 'block'); 
           $('.header__close').css('display', 'none');
           $('.header__nav').css('display', 'none');
        }
    });
});

// slick

$('.single-item').slick({
  centerPadding: '0'
});


